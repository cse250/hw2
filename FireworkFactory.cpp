#include "FireworkFactory.h"

FireworkFactory::FireworkFactory(){

}

// Receive a stack of Firework pointers and add them to the factories inventory. Fireworks must be sold in the order
// that they are popped off the stack (The top of the stack is the oldest firework).
void FireworkFactory::fireworkShipment(stack<Firework*>& fireworkShipment){

    while (!fireworkShipment.empty()){
      inventory.push(fireworkShipment.top());
      fireworkShipment.pop();
    }
  }


// Sell quantity fireworks by pushing them onto the customerStack with the oldest fireworks being sold first (FIFO).
void FireworkFactory::sellFireworks(stack<Firework*>& customerStack, int quantity){
    if (!inventory.empty()){
      for (int i = 0; i < quantity; i++){
        customerStack.push(inventory.front());
        inventory.pop();
      }
    }
    else{
      std::cout << "Sorry, we are out of fireworks. Please try again tomorrow." << std::endl;
    }
}

// Before destroying the factory you must properly dispose of all the fireworks in your inventory (On the heap).
FireworkFactory::~FireworkFactory(){


    while (!inventory.empty()){
      Firework *del = inventory.front();
      delete del;
      inventory.pop();
    }

}

// Receive a shipment of metal which will be used to make new fireworks in the factory. Whenever the factory has 5 of
// any type of metal it must immediately make a firework of the corresponding color and add it to the inventory.
// To make a firework, you must use the new keyword to create it dynamically on the heap and manage a pointer to
// the firework.
void FireworkFactory::metalShipment(stack<Metal>& metalShipment){
    while (!metalShipment.empty()){
        if (metalShipment.top() == 0){
          orangeFW.push(metalShipment.top());
          metalShipment.pop();
          if (orangeFW.size() >= 5){
            Firework* forange = new Firework(Orange);
            inventory.push(forange);
            while(!orangeFW.empty()){
              orangeFW.pop();
        }
      }
    }
        else if(metalShipment.top() == 1){
          greenFW.push(metalShipment.top());
          metalShipment.pop();
          if (greenFW.size() >= 5){
            Firework* fgreen = new Firework(Green);
            inventory.push(fgreen);
            while(!greenFW.empty()){
              greenFW.pop();
        }
      }
    }
        else if(metalShipment.top() == 2){
          blueFW.push(metalShipment.top());
          metalShipment.pop();
          if (blueFW.size() >= 5){
            Firework* fblue = new Firework(Blue);
            inventory.push(fblue);
            while(!blueFW.empty()){
              blueFW.pop();
        }
      }
    }
        else if(metalShipment.top() == 3){
          purpleFW.push(metalShipment.top());
          metalShipment.pop();
          if (purpleFW.size() >= 5){
            Firework* fpurple = new Firework(Purple);
            inventory.push(fpurple);
            while(!purpleFW.empty()){
              purpleFW.pop();
        }
      }
    }
        else {
          std::cout << "There is no metal." << std::endl;
        }
      }
    }

// A customer is purchasing quantity fireworks, but they must all be of the color specified by the customer. The order
// in which the fireworks are sold must be maintained (FIFO), but fireworks of the specified color can be sold before
// other colors if there is no other way to fulfil the order. Be sure not to sell the same firework more than once with
// either of the sellFireworks functions.
//
// For the efficient point of this assignment, this function must run in O(quantity) time. If only 1 firework is being
// purchased, the runtime must be O(1) regardless of where the firework to be sold is in the inventory.
void FireworkFactory::sellFireworks(stack<Firework*>& customerStack, int quantity, Color color){
  queue<Firework*> temp;
  int i = 0;
  if (!inventory.empty()){
    while (i != quantity){
      Firework *current = inventory.front();
      if(current->getColor() == color){
        customerStack.push(inventory.front());
        inventory.pop();
        i++;
      }
      else{
          temp.push(inventory.front());
          inventory.pop();
    }
  }
      while(!inventory.empty()){
        temp.push(inventory.front());
        inventory.pop();
      }
      while(!temp.empty()){
        inventory.push(temp.front());
        temp.pop();
      }
    }
  else{
    std::cout << "Sorry we don't have any fireworks of that color." << std::endl;
  }
}
